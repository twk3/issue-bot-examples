# issue-bot-examples

Example issues created in CI via https://gitlab.com/mnielsen/issue-bot.

- [Example issue](https://gitlab.com/mnielsen/issue-bot-examples/-/issues/17)
- [Example job that created an issue](https://gitlab.com/mnielsen/issue-bot-examples/-/jobs/1470613555)
- [Example job that skipped creating an issue](https://gitlab.com/mnielsen/issue-bot-examples/-/jobs/1470615508)
- [Example job that reopened an issue](https://gitlab.com/mnielsen/issue-bot-examples/-/jobs/1470617188)
